# BS|ENERGY Building Blocks

Diese Repo enthält Komponenten, zum Bau unserer Webapplikationen.

## Wie Du die Komponenten nutzen kannst?
* Installieren mit NPM: `npm install bseblocks`
* Importieren von Vue-App: 
  ```js
  //App.vue
  <script setup>
    import DiagrammBarChartJS from 'bseblocks/components/DiagrammBarChartJS.vue';
  <script>
  ```

* Einbinden der Komponente: 
  ```js
  //App.vue
  <template>
    <DiagrammBarChartJS/
    ...
    >
  <template>
  ```

## nhalte

### Handlungen
* Datepicker.vue - Datum erfassen
* FotoUpload.vue - Foto hochladen

### Diagramme
* DiagrammSankey.vue Sankey-Diagramm mit Plotly
* DiagrammBarPlotly.vue -  Balken-Diagramm mit Plotly
* DiagrammBarChartJS.vue - Balken-Diagramm mit ChartJS

## Weitere Informationen
* https://www.bs-energy.de/
* ...